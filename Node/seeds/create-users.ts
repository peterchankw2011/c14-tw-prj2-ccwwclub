import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("users").del();
  // await knex("room_participants").del()
  // await knex("rooms").del()

  // Inserts seed entries
  await knex("users").insert([
    { email: "justinb8@email.com", password: "123", nickname: "JustinB8" },
    { email: "justinb7@email.com", password: "123", nickname: "JustinB7" },
    // { email: "justin@gmail.com", pas}
  ]);
  // await knex("rooms").insert([
  //   { creator_id: 3, password: "12345", description: "Professional drawer in", accommodation:3, status:'pending', mode: 'normal', round:3, time_limit:10, nth_draw:0 },
  // ]);
}
