from sanic import Sanic
from sanic import response
from sanic.response import json
# from sanic_sse import Sse
import base64
import cv2
import numpy as np
from PIL import Image
import io
import HandTrackingModule as htm

from sanic.response import text
from sanic_cors import CORS, cross_origin
import socketio

# from gauganTest import evaluate, to_image



sio = socketio.AsyncServer(
    async_mode='sanic', cors_allowed_origins=[])
# app = Sanic("App Name")

# This function is optional callback before sse request
# You can use it for authorization purpose or something else


async def before_sse_request(request):
    if request.headers.get("Auth", "") != "some_token":
        abort(HTTPStatus.UNAUTHORIZED, "Bad auth token")

app = Sanic(__name__)
# sio.attact(app)
app.config['CORS_SUPPORTS_CREDENTIALS'] = True
CORS(app)
sio.attach(app)

# The default sse url is /sse but you can set it via init argument url.
# Sse(app, url="/events", before_request_func=before_sse_request)  # or you can use init_app method

def keras_process_image(img):
    image_x = 28
    image_y = 28
    img = cv2.resize(img, (image_x, image_y))
    img = cv2.GaussianBlur(img,(3,3),0)
    img = np.array(img, dtype=np.float32)
    img = np.reshape(img, (-1, image_x, image_y, 1))
    img /= 255.0
    return img


class Room:

    def __init__(self, participantList, round, timeLimit):
        self.participantList = participantList
        self.round = round
        self.timeLimit = timeLimit
        self.timePerRound = self.timeLimit*len(participantList)
        self.startTime = 0

    def startGame(self):
        self.startTime = datetime.now()

    def currentDrawer(self):
        duration = (datetime.now() - self.startTime).total_seconds()
        currentDrawerId = participantList[(duration % self.timePerRound)]
        return currentDrawerId


class Canvas:
    canvas_dict = {}

    def __init__(self, drawerId, roomId, nthDraw, brushThickness=10, eraserThickness=100):
        self.id = 'room{}_{}th'.format(roomId, nthDraw)
        self.drawerId = drawerId
        self.brushThickness = brushThickness
        self.eraserThickness = eraserThickness
        self.xp, self.yp = 0, 0
        self.imgCanvas = np.zeros((360, 360, 3), np.uint8)
        # self.gauGanCanvas = np.full((720, 720, 3), (223,158,117), np.uint8) # B,G,R sky 223 158 117
        self.detector = htm.handDetector(detectionCon=0.65, maxHands=1)
        self.drawLine = []
        self.img_pic_str = ''
        self.ccww_pic_str = ''
        Canvas.canvas_dict[self.id] = self

    def selectColor(self, colorKey):
        return {
            # '0': (0, 0, 0),
            # '1': (255, 0, 255),
            # '2': (255, 0, 0),
            # '3': (0, 255, 0)
            '0': (0, 0, 0),
            '1': (93, 110, 50),
            '2': (255, 0, 0),
            '3': (0, 255, 0)
        }[colorKey]

    def canvasDrawing(self, dataURL, colorKey, gauGan=False):
        overlayList = []
        drawColor = self.selectColor(colorKey)
        image_b64 = dataURL.split(',')[1]
        binary = base64.b64decode(image_b64)
        img = Image.open(io.BytesIO(binary))  # this is RGBA
        img = img.convert('RGB')  # convert RGBA to RGB
        b, g, r = img.split()
        img = Image.merge("RGB", (r, g, b))
        img = np.array(img)
        img = cv2.flip(img, 1)

        # 2. Find Hand Landmarks
        img = self.detector.findHands(img)  # draw hand landmarks
        lmList = self.detector.findPosition(
            img, draw=False)  # find position of hand marks
        if len(lmList) != 0:
            # tip of index and middle fingers
            x1, y1 = lmList[8][1:]
            x2, y2 = lmList[12][1:]
            # 3. Check which fingers are up
            fingers = self.detector.fingersUp()
            # 4. If Selection Mode - Two finger are up
            if fingers[1] and fingers[2]:
                if self.drawLine != []:  # keep drawing a line
                    print(self.drawLine)
                    self.drawLine = []
                self.xp, self.yp = 0, 0
                cv2.rectangle(img, (x1, y1 - 25), (x2, y2 + 25),
                              drawColor, cv2.FILLED)
            # 5. If Drawing Mode - Index finger is up
            if fingers[1] and fingers[2] == False:
                cv2.circle(img, (x1, y1), 15, drawColor, cv2.FILLED)
                # print("Drawing Mode")
                # print("{},{}".format(x1,y1))
                # record new point of drawing line
                self.drawLine.append([x1, y1])
                if self.xp == 0 and self.yp == 0:
                    self.xp, self.yp = x1, y1
                cv2.line(img, (self.xp, self.yp), (x1, y1),
                         drawColor, self.brushThickness)
                if drawColor == (0, 0, 0):
                    cv2.line(img, (self.xp, self.yp), (x1, y1),
                             drawColor, self.eraserThickness)
                    cv2.line(self.imgCanvas, (self.xp, self.yp), (x1, y1),
                             drawColor, self.eraserThickness)
                    # cv2.line(self.gauGanCanvas, (self.xp, self.yp), (x1, y1),
                    #          drawColor, self.eraserThickness)
                else:
                    cv2.line(img, (self.xp, self.yp), (x1, y1),
                             drawColor, self.brushThickness)
                    cv2.line(self.imgCanvas, (self.xp, self.yp), (x1, y1),
                             drawColor, self.brushThickness)
                    # cv2.line(self.gauGanCanvas, (self.xp, self.yp), (x1, y1),
                    #          drawColor, self.brushThickness)
                self.xp, self.yp = x1, y1
            # # Clear Canvas when all fingers are up
            # if all (x >= 1 for x in fingers):
            #     imgCanvas = np.zeros((720, 1280, 3), np.uint8)
            # Clear Canvas when gesture are up
            if (fingers[1] and fingers[2] == False) and fingers[3]:
                imgCanvas = np.zeros((720, 1280, 3), np.uint8)
        imgGray = cv2.cvtColor(self.imgCanvas, cv2.COLOR_BGR2GRAY)
        br = imgGray
        _, imgInv = cv2.threshold(imgGray, 50, 255, cv2.THRESH_BINARY_INV)
        imgInv = cv2.cvtColor(imgInv, cv2.COLOR_GRAY2BGR)
        bw = imgInv
        img = cv2.bitwise_and(img, imgInv)
        im = img
        img = cv2.bitwise_or(img, self.imgCanvas)
        # Setting the header image
        # img[0:125, 0:1280] = header
        # img = cv2.addWeighted(img,0.5,imgCanvas,0.5,0)
        # cv2.imshow("Image", img)
        # cv2.imshow("Canvas", imgCanvas)
        # cv2.imshow("Inv", imgInv)
        cv2.waitKey(1)

        # gaugan = self.imgCanvas
        # black_pixels = np.where(
        #     (gaugan[:,:,0]==0)&
        #     (gaugan[:,:,0]==0)&
        #     (gaugan[:,:,0]==0)
        # )

        # gaugan[black_pixels] = [56,79,131]
        # gaugan = cv2.floodFill(self.imgCanvas,(56,79,131),(0,0,0),(0,0,0))

        # print(bw)
        # numpy array img to base 64
        retval, buffer = cv2.imencode('.png', img)
        pic_str1 = base64.b64encode(buffer)
        pic_str1 = pic_str1.decode()
        self.img_pic_str = pic_str1

        retval, buffer = cv2.imencode('.png', bw)
        pic_str2 = base64.b64encode(buffer)
        pic_str2 = pic_str2.decode()
        self.ccww_pic_str = pic_str2

        

        # retval, buffer = cv2.imencode('.png', br)  # numpy array img to base 64
        # pic_str2 = base64.b64encode(buffer)
        # pic_str2 = pic_str2.decode()
        # return [pic_str1, pic_str2]
        return [pic_str1, pic_str2]

    def viewPaint(self):
        return self.img_pic_str



# @sio.on('imageURL')
# async def print_message(sid, message):

#     dataURL = message['img']
#     drawerId = message['drawerId']
#     colorKey = message['colorKey']
# dataURL = message['img']  # data: image/png:base64, iVBORw0KGgo.....
# drawerId = request.json['drawerId']
# colorKey = request.json['colorKey']
# drawerString = "drawer{}".format(drawerId)
# if drawerId not in Canvas.drawers_dict:
#     globals()[drawerString] = Canvas(drawerId)
# base64_img = globals()[drawerString].canvasDrawing(dataURL, colorKey)
# await sio.emit('imageURL',  {'img_base64': base64_img})
# await sio.emit('imageURL',  {'img_base64': dataURL})


@app.route("/paint", methods=['POST'])
async def paint(request):
    dataURL = request.json['img']  # data: image/png:base64, iVBORw0KGgo.....
    drawerId = request.json['drawerId']
    roomId = request.json['roomId']
    nthDraw = request.json['nthDraw']
    colorKey = request.json['colorKey']
    canvasObjectName = "room{}_{}th".format(roomId, nthDraw)
    if canvasObjectName not in Canvas.canvas_dict:
        globals()[canvasObjectName] = Canvas(drawerId, roomId, nthDraw)
    gauGan = request.json['gauGan']
    base64_img = globals()[canvasObjectName].canvasDrawing(dataURL, colorKey, gauGan)

    # @sio.on('imageURL')
    # async def send(sid, message):
    #     await sio.emit('imageURL',  {'img_base64': base64_img[0]})

    return response.json(
        {'img_base64': base64_img},
        status=200
    )


@app.route("/view", methods=['POST'])
async def view(request):
    drawerId = request.json['drawerId']
    roomId = request.json['roomId']
    nthDraw = request.json['nthDraw']
    canvasObjectName = "room{}_{}th".format(roomId, nthDraw)
    # drawerString = "drawer{}".format(drawerId)
    base64_img = globals()[canvasObjectName].viewPaint()

    # channel_id = request.json.get(request.json['drawerId'])
    # try:
    #     await request.app.sse_send(json_dumps(request.json), channel_id=channel_id)
    # except KeyError:
    #     abort(HTTPStatus.NOT_FOUND, "channel not found")

    return response.json(
        {'img_base64': base64_img},
        status=200
    )

# template = env.get_template('./templates/index.html')
# content = template.render(links=await )

# @app.route("/gaugan", methods=['POST'])
# def generate(request):
#     print('Testing')
#     # labelmap = np.load('/app/labelmap.npy')
#     labelmap = np.asarray(request.json)
#     print(labelmap)

#     image = evaluate(labelmap)
#     image = to_image(image)

#     # create file-object in memory
#     file_object = io.BytesIO()
#     # write PNG in file-object
#     image.save(file_object, 'PNG')
#     # move to beginning of file so `send_file()` it will read from start    
#     file_object.seek(0)
#     print('Testing')

#     return send_file(file_object, mimetype='image/PNG')


@app.route('/')
async def index(request):
    """Video streaming home page."""
    return response.html('<div>test</div>')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
