import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('room_participants', table=>{
        table.increments();
        table.integer('roomId').references('id').inTable('rooms')
        table.integer('userId').references('id').inTable('users')
        table.string('role',255);
        table.decimal('scores',5,2)
        table.timestamps(false,true);
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('room_participants');
}

