import express from "express";
import { Request, Response } from "express";
import {
  upload,
  userController,
  roomController,
  gameController,
} from "./main";
import { isLoggedInAPI } from './guards'

export const routes = express.Router();

routes.get("/", function (req: Request, res: Response) {
  res.redirect("/index");
});

//user routes
routes.post("/login", userController.login);
routes.post("/register", upload.single("pro_pic"), userController.register);
routes.get("/login/google", userController.loginGoogle);
routes.get("/userInfo",isLoggedInAPI, userController.userInfo);
routes.get("/logout", isLoggedInAPI, userController.logout)

//room routes
routes.get("/getRooms", isLoggedInAPI, roomController.allRooms);
routes.post("/createRoom", isLoggedInAPI, roomController.createRoom);
routes.post("/checkPW/:id", isLoggedInAPI, roomController.checkPW);
routes.get("/userJoinsRoom/:id", isLoggedInAPI, roomController.userJoinRoom);
routes.get("/joinRoomAfterCreate", isLoggedInAPI, roomController.joinRoomAfterCreate);
routes.get("/getPartiInRoom/:id", isLoggedInAPI, roomController.getPartiInRoom);
routes.get("/getAccommodation/:id", isLoggedInAPI, roomController.getAccommodation);
routes.get("/leaveRoom/:roomId", isLoggedInAPI, roomController.leaveRoom)

//game routes
routes.get("/roomPlayers/:roomId", isLoggedInAPI, gameController.allPlayer);
routes.get("/currentDrawingStatus/:roomId", isLoggedInAPI, gameController.drawingStatus);
routes.get("/startDrawing/:roomId", isLoggedInAPI, gameController.startDrawing);
routes.get("/remainingTime/:roomId", isLoggedInAPI, gameController.remainingTime);
routes.post("/guessChecking/:roomId", isLoggedInAPI, gameController.guessChecking);
routes.get("/playerScores/:roomId", isLoggedInAPI, gameController.playerScores);
// routes.get("/AIPrediction", isLoggedInAPI, gameController.AIPrediction)
routes.post("/skipDisconnectedPlayer/:roomId", isLoggedInAPI, gameController.skipDisconnectedPlayer);
