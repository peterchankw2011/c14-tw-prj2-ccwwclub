import { Request, Response } from 'express';
import { User } from '../services/models';
import { UserService } from '../services/userService';


export class UserController {
    constructor(private userService: UserService) { };

    login = async (req: Request, res: Response) => {
        const checkUser = await this.userService.checkLoginInfo(req.body);
        if (checkUser) {
            if (req.session) {
                req.session['user'] = {
                    id: checkUser['id'],
                    nickname: checkUser['nickname'],
                    pro_pic: checkUser['pro_pic']
                }
            }
            return res.status(200).redirect('/lobby')
        } else {
            return res.status(401).send({ message: "Wrong email/password" })
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.['grant'].response.access_token;
        const result = await this.userService.checkGoogleToken(accessToken);
        const checkUser = await this.userService.checkUserExist(result.email);

        if (checkUser) {
            if (req.session) {
                req.session['user'] = {
                    id: checkUser['id'],
                    nickname: checkUser['nickname'],
                    pro_pic: checkUser['pro_pic']
                }
            }
            return res.status(200).redirect('/lobby')
        } else if (!checkUser) {
            const registerUser: User = {
                email: result.email,
                password: result.id,
                nickname: result.name
            }
            const addedUser = await this.userService.addUser(registerUser)
            if (req.session) {
                req.session['user'] = {
                    id: addedUser[0]['id'],
                    nickname: addedUser[0]['nickname'],
                    pro_pic: addedUser[0]['pro_pic']
                }
            }
            return res.status(200).redirect('/lobby')
        } else {
            return res.status(401).send({ success: "false" })
        }
    }


    register = async (req: Request, res: Response) => {
        if (await this.userService.checkUserExist(req.body['email'])) {
            return res.status(401).send({ message: "Email already registered" })
        } else if (await this.userService.checkNickname(req.body['nickname'].trim())) {
            return res.status(401).send({ message: "nickname already been taken" })
        } else if (!(req.body['password'] === req.body['confirm_password'])) {
            return res.status(401).send({ message: "password not matching" })
        } else {
            const addedUser = await this.userService.addUser(req.body, req.file)
            if (addedUser) {
                if (req.session) {
                    req.session['user'] = {
                        id: addedUser[0]['id'],
                        nickname: addedUser[0]['nickname'],
                        pro_pic: addedUser[0]['pro_pic']
                    }
                }
                return res.status(200).send({ message: "Successful register" })
            } else {
                return res.status(401).send({ message: "Error" })
            }
        }
    }

    logout = async (req: Request, res: Response) => {
        if (req.session) {
            delete req.session['user'];
        }
        res.status(200).send({message: "Successfully logged out!"})
    }

    userInfo = async (req: Request, res: Response) => {
        if (req.session) {
            const userInfo = await this.userService.getUserInfo(req.session['user'].id)
            return res.status(200).send({userInfo: userInfo})
        }else{
            return res.status(400).send({message: "Error"})
        }
    }
}