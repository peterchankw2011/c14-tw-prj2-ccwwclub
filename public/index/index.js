function loginToRegister (){
    const switchBtn = document.querySelector('#switch-button')
    switchBtn.addEventListener("click", (event) => {
        const registerInfos = document.querySelectorAll('.form-container')
        for (registerInfo of registerInfos){
            registerInfo.classList.toggle("hidden")
        }
        if(event.target.innerHTML == "Create Account"){
            event.target.innerHTML = "Return to login"
        }else {
            event.target.innerHTML = "Create Account"
        }
      });
}

loginToRegister();

document.querySelector('#login-form').addEventListener('submit', async function(event){
    event.preventDefault();

    //get form information
    const form = event.target;
    const loginInfo = {};
    loginInfo['email'] = form.email.value;
    loginInfo['password'] = form.password.value;

    const res = await fetch('/login', {
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify(loginInfo)
    });
    

    if(res.status === 200){
        window.location = "/lobby"
    }else{
        const result = await res.json();
        alert(result.message);
        form.reset();
    }
    
})

document.querySelector('#register-form').addEventListener('submit', async function(event){
    event.preventDefault();

    //get form information
    const form = event.target;
    const registerInfo = new FormData();

    registerInfo.append('email', form.email.value);
    registerInfo.append('nickname', form.nickname.value);
    registerInfo.append('password', form.password.value);
    registerInfo.append('confirm_password', form.confirmPassword.value);
    registerInfo.append('pro_pic', form.img.files[0]);

    const res = await fetch('/register', {
        method:"POST",
        body:registerInfo
    });
    
    if(res.status === 200){
        const result = await res.json();
        alert(result.message)
        form.reset();
        window.location = "/index"
    }else{
        const result = await res.json();
        alert(result.message)
        form.reset();
    }
    
})



