import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('rooms',(table)=>{
        table.renameColumn('creatorId','creator_id');
    })
    await knex.schema.alterTable('room_participants',(table)=>{
        table.renameColumn('roomId','room_id');
        table.renameColumn('userId','user_id');
    })
    await knex.schema.alterTable('room_requests',(table)=>{
        table.renameColumn('roomId','room_id');
        table.renameColumn('userId','user_id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('rooms',(table)=>{
        table.renameColumn('creator_id','creatorId');
    })
    await knex.schema.alterTable('room_participants',(table)=>{
        table.renameColumn('room_id','roomId');
        table.renameColumn('user_id','userId');
    })
    await knex.schema.alterTable('room_requests',(table)=>{
        table.renameColumn('room_id','roomId');
        table.renameColumn('user_id','userId');
    })
}

