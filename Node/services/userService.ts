import { UserLogin, User } from './models';
import { Knex } from 'knex'
import { hashPassword, checkPassword } from '../hash'
import fetch from 'node-fetch'


export class UserService {
    constructor(private knex: Knex){}

    async checkUserExist(email: string){
        const userExist = await this.knex
                            .select("*")
                            .from("users")
                            .where("email", email)
        return userExist[0]? userExist[0]:false
    }

    async checkNickname(nickname: string){
        const userExist = await this.knex
        .select("*")
        .from("users")
        .where("nickname", nickname)
        return userExist[0]? userExist[0]:false
    }

    async checkLoginInfo(body: UserLogin){
        const {email, password} = body;
        const checkUser = await this.checkUserExist(email)
        if(checkUser){
            //check if password matches
            const  match = await checkPassword(password, checkUser['password'])
            if(match){
                return checkUser
            }else{
                return false
            }
        }else{
            return false
        }
    }

    async checkGoogleToken (accessToken: string){
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
        method:"get",
        headers:{
            "Authorization":`Bearer ${accessToken}`
         }});
         return await fetchRes.json()
    }

    async addUser(body: User, file?: Express.Multer.File) {
        const hashedPassword = await hashPassword(body.password)
        const result = await this.knex
                             .insert({
                                 email: body.email,
                                 password: hashedPassword,
                                 nickname: body.nickname,
                                 pro_pic: file?.filename
                             })
                             .into("users")
                             .returning(["id","nickname","pro_pic"])
        return result
    }

    async getUserInfo(userId:string){
        const result = await this.knex.select('*').from('users').where('id', userId)
        return result
    }
}