import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable('rooms', table=>{
        table.integer('nth_draw')
        table.string('guest_item',255)
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable('rooms', table=>{
        table.dropColumn('nth_draw')
        table.dropColumn('guest_item')
        
    })
}

