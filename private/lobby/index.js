const socket = io();
const createRoomForm = document.querySelector(".createRoomForm");
window.onpageshow = () => {
  getUserInfo();
  getRooms();
};

createRoomForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const form = e.target;
  const formObject = {};

  formObject["roomName"] = form.roomname.value;
  formObject["enableAI"] = form.enableAI.value;
  formObject["numberOfPeople"] = form.numberOfPeople.value;
  formObject["roundsPerPerson"] = form.roundsPerPerson.value;

  if (form.privacy.value === "private") {
    formObject["password"] = form.password.value;
  }
  if (form.privacy.value === "public") {
    formObject["password"] = "";
  }

  const res = await fetch(`/createRoom`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(formObject),
  });

  const result = await res.json();

  socket.emit("createSubmit", "activated refresh lobby");
  joinRoomAfterCreate();
  form.reset();
});

const private = document.querySelector("#privacy");
const submit = document.querySelector("#submitForm");
const passwordDiv = document.createElement("div");
passwordDiv.setAttribute("id", "passwordDiv");

private.addEventListener("change", (event) => {
  if (event.target.value === "private") {
    passwordDiv.innerHTML = ``;
    passwordDiv.innerHTML = ` <label for="enterPassword">Room Password:</label><input id="enterPassword" name="password" type="password"> </input>`;
    createRoomForm.insertBefore(passwordDiv, submit);
  }
  if (event.target.value === "public") {
    passwordDiv.innerHTML = ``;
  }
});

async function joinRoomAfterCreate() {
  const res = await fetch(`/joinRoomAfterCreate`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;
    const roomId = data.rows[0].id;
    window.location = `/room/room.html?roomId=${roomId}`;
  }
}

const lobbyDiv = document.querySelector(".roomContainer");

socket.on("createSubmit", (data) => {
  getRooms();
});

async function getRooms() {
  lobbyDiv.innerHTML = ` `;
  const res = await fetch(`/getRooms`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;

    for (let i = 0; i < data.rows.length; i++) {
      const res1 = await fetch(`/getPartiInRoom/${data.rows[i].roomid}`);
      const data1 = await res1.json();
      const roomDiv = document.createElement("div");
      roomDiv.setAttribute("class", "room");
      roomDiv.setAttribute(
        "pwornot",
        `${data.rows[i].password == "" ? `Public` : `Private`}`
      );
      roomDiv.setAttribute("id", `${data.rows[i].roomid}`);
      roomDiv.innerHTML = `<img class="roomProPic" src="../uploads/${
        data.rows[i].propic
      }" />
      <div class="roomAndHost">
        <div class="roomName">${data.rows[i].name}</div>
        <div class="hostName">Host: ${data.rows[i].nickname}</div>
      </div>
      <div class="privateInicator">${
        data.rows[i].password == "" ? `Public` : `Private`
      }</div>
      <div class="aiOrNot">A.I: ${data.rows[i].mode}</div>
      <div class="numberOfPeople">
        <div class="peopleInRoom">People in room:</div>
        <div class="number">${data1.rows[0].count} / ${
        data.rows[i].accommodation
      } <span>(RPP: ${data.rows[i].round})</span></div>
      </div>`;
      lobbyDiv.appendChild(roomDiv);
    }
    let rooms = document.querySelectorAll(".room");

    function getsIntoRoom(id) {
      window.location = `/room/room.html?roomId=${id}`;
    }
    for (room of rooms) {
      room.addEventListener("click", (event) => {
        if (event.currentTarget.getAttribute("pwornot") === "Public") {
          let id = event.currentTarget.getAttribute("id");
          async function numberComparing(id) {
            const resParti = await fetch(`/getPartiInRoom/${id}`);
            const dataParti = await resParti.json();
            const parti = dataParti.rows[0].count;
            const resAccom = await fetch(`/getAccommodation/${id}`);
            const dataAccom = await resAccom.json();
            const accom = dataAccom.rows[0].accommodation;
            if (parti < accom) {
              userJoinsRoom(id);
              socket.emit("createSubmit", "activated refresh lobby");
              getsIntoRoom(id);
            } else {
              console.log(`the room is full`);
            }
          }
          numberComparing(id);
        }
        if (event.currentTarget.getAttribute("pwornot") === "Private") {
          let id = event.currentTarget.getAttribute("id");
          async function numberComparingPrivate(id) {
            const resParti = await fetch(`/getPartiInRoom/${id}`);
            const dataParti = await resParti.json();
            const parti = dataParti.rows[0].count;
            const resAccom = await fetch(`/getAccommodation/${id}`);
            const dataAccom = await resAccom.json();
            const accom = dataAccom.rows[0].accommodation;
            if (parti < accom) {
              document.querySelector(".passwordLogin").style.display = "flex";
              document
                .querySelector("#joinRoomForm")
                .addEventListener("submit", async (e) => {
                  e.preventDefault();
                  const form = e.target;
                  const formObject = {};
                  formObject["password"] = form.roomLoginPw.value;

                  const res = await fetch(`/checkPW/${id}`, {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify(formObject),
                  });

                  const result = await res.json();
                  if (result.success === true) {
                    userJoinsRoom(id);
                    socket.emit("createSubmit", "activated refresh lobby");
                    getsIntoRoom(id);
                  }
                  if (result.success === false) {
                    document.querySelector("#roomLoginPw").style[
                      "background-color"
                    ] = "#d81d1d";
                  }
                  form.reset();
                });
              console.log("private!!");
            } else {
              console.log(`the room is full`);
            }
          }
          numberComparingPrivate(id);
        }
      });
    }
  }
}

function closeRoomLogin() {
  document.querySelector(".passwordLogin").style.display = "none";
}

async function userJoinsRoom(id) {
  const res = await fetch(`/userJoinsRoom/${id}`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;
  }
}

async function getUserInfo() {
  const res = await fetch(`/userInfo`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;

    const nickname = data.userInfo[0].nickname;
    const propic = data.userInfo[0].pro_pic;
    const user = document.querySelector(".user");
    user.innerHTML = `<img class="propic" src="../uploads/${propic}" />
    <div class="user-name">${nickname}</div>`;
  }
}
const logout = document.querySelector(".logout");
logout.addEventListener("click", async (event) => {
  const res = await fetch("/logout");
  const result = await res.json();
  if (res.status === 200) {
    alert(result.message);
    window.location = "/index";
  }
});
