import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable('rooms', table=>{
        table.timestamp('started_at')
        table.integer('round')
        table.integer('time_limit')
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable('rooms', table=>{
        table.dropColumn('started_at')
        table.dropColumn('round')
        table.dropColumn('time_limit')
    })
}

