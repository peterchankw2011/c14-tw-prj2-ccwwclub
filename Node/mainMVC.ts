import express from "express";
import expressSession from "express-session";
import http from "http";
import path from "path";
import { Server as SocketIO } from "socket.io";
import { Client } from "pg";
import KnexFunction from "knex";
import dotenv from "dotenv";
import multer from "multer";
import grant from "grant";
import { UserController } from "./controllers/userController";
import { UserService } from "./services/userService";
import { RoomController } from "./controllers/roomController";
import { RoomService } from "./services/roomService";
import { GameController } from "./controllers/gameController";
import { GameService } from "./services/gameService";

import { isLoggedIn } from "./guards";

dotenv.config();

//Configure multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("../public/uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});
export const upload = multer({ storage });

//Connect to pg sql through knex
const knexfile = require("./knexfile");
const configEnv = process.env.NODE_ENV || "development";
export const knex = KnexFunction(knexfile[configEnv]);

//Connect to pg sql
export const client = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

client.connect();

//Initialize socketIO
const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);

//Prepare the Services and Controllers
const userService = new UserService(knex);
export const userController = new UserController(userService);
const roomService = new RoomService(knex, io);
export const roomController = new RoomController(roomService);
const gameService = new GameService(knex, io);
export const gameController = new GameController(gameService);

//Setup API Routes
import { routes } from "./routes";

//parse contents
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//Initialize session
app.use(
  expressSession({
    secret: "CCWW is fun",
    resave: true,
    saveUninitialized: true,
  })
);

//Create SocketIO function
const users = {}
io.on("connection", function (socket) {
  console.log(socket.id);
  socket.on("joinRoom", (roomId) => {
    socket.join(roomId);
    socket.on("playerCamera", (cameraInfo) => {
      // streaming for players camera updating
      io.to(roomId).emit("playerCamera", cameraInfo);
    });
    socket.on("roomConnect", (userId) => {
      console.log("user "+userId+" connected")
      users[socket.id] = userId
      const connectMsg = "User " + userId + " connected"
      gameService.participantReconnect(userId, roomId)
      io.to(roomId).emit("roomConnect", {userId: userId, msg: connectMsg})
    })
    socket.on("roomDisconnect", () => {
      delete users[socket.id]
      const disconnectMsg = "User " + users[socket.id] + " disconnected"
      gameService.participantDisconnect(users[socket.id], roomId)
      io.to(roomId).emit("roomDisconnect", {userId: users[socket.id], msg: disconnectMsg})
    });
  });
  socket.on("createSubmit", (data) => {
    console.log(data);
    io.emit("createSubmit", "refresh!");
  });
});

//Setup google oauth config
const grantExpress = grant.express({
  defaults: {
    origin: "http://localhost:8080",
    transport: "session",
    state: true,
  },
  google: {
    key: process.env.GOOGLE_CLIENT_ID || "",
    secret: process.env.GOOGLE_CLIENT_SECRET || "",
    scope: ["profile", "email"],
    callback: "/login/google",
  },
});

app.use(grantExpress as express.RequestHandler);

app.use("/", routes);
// Setup Static Files
app.use(express.static("../public"));
app.use(isLoggedIn, express.static("frontend"));

app.use((req, res) => {
  res.status(404).redirect("/404");
});

const PORT = 8080;
server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
