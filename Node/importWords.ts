import KnexFunction from "knex";


const knexfile = require('./knexfile');// 呢個位一定要寫require
// // 呢個細階knex就好似我地 pg 入面嘅client咁
const configEnv = process.env.NODE_ENV || "development";
const knex = KnexFunction(knexfile[configEnv]);

const fs = require('fs')
fs.readFile('./classes.csv', 'utf8', async function (err: any, data: string) {
    let itemWords = data.split(',');
    let word = ''
    for (word of itemWords){
        await knex("words").insert({ english: word});
    }
})