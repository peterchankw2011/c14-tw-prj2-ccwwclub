select*from rooms;
select*from room_participants;
select*from users;
select*from words;

SELECT rooms.round, COUNT(room_participants.id)
                                  FROM rooms, room_participants
                                  WHERE rooms.id = 1 OR room_participants.room_id = 1
                                  GROUP BY rooms.id

UPDATE room_participants SET user_id = 3, role = 'creator' where id = 1
UPDATE room_participants SET user_id = 1, role = 'participant' where id =3
UPDATE rooms SET status = 'pending' where id = 1

DELETE from room_participants where id in (41,42,43);
DELETE from rooms where id = 1

select * from users where nickname = 'JustinWick'

select*from room_participants where room_id = 2

SELECT status FROM rooms WHERE id = 8

DELETE from users
DELETE from room_participants
DELETE from rooms

SELECT room_participants.scores, users.nickname FROM room_participants
                                                    INNER JOIN users ON room_participants.user_id = users.id
                                                    WHERE room_participants.room_id = 51