import { Request, Response } from 'express';
import { GameService } from '../services/gameService';


export class GameController {
  constructor(private gameService: GameService) { };

  allPlayer = async (req: Request, res: Response) => {
    const roomId = req.params.roomId;
    //const roomId = parseInt(req.param.roomId);
    const players = await this.gameService.getAllPlayers(roomId);
    return res.json(players)
  }

  skipDisconnectedPlayer = async (req: Request, res: Response) => {
    const roomId = req.params.roomId
    const nthDraw = req.body.nthDraw
    await this.gameService.skipDisconnectedPlayer(roomId, nthDraw)
    this.gameService.sendSkipDisconnectedPlayerMsg(roomId)
    return res.json({disconnected:'disconnected player skipped'})
  }

  checkDisconnect = async (roomId: string) => {
    let data = await this.gameService.getRoomInfo(roomId)
    let participants = await this.gameService.getParticipants(roomId)
    let nthDraw = data[0]['nth_draw']
    let currentDrawerNthPlayer = Math.floor(nthDraw % participants.length)
    let currentDrawerId = participants[currentDrawerNthPlayer].user_id
    let drawerIsDisconnect = false
    if(participants[currentDrawerNthPlayer].role==='disconnected'){
      drawerIsDisconnect = true
    }

    let totalRounds = data[0]['round']
    let remainingDraw = totalRounds * participants.length - nthDraw
    let remainingRounds = Math.floor(remainingDraw / participants.length)
    let drawingStatus = data[0]['status']
    let guessWord = data[0]['guess_item']
    let endGame = false
    if (nthDraw >= participants.length*totalRounds){
      endGame = true
    }
    return { endGame: endGame, drawerId: currentDrawerId, drawerIsDisconnect: drawerIsDisconnect, nthDraw: nthDraw, totalRounds: totalRounds, remainingRounds: remainingRounds, drawingStatus: drawingStatus, guessWord: guessWord }
  }

  drawingStatus = async (req: Request, res: Response) => {
    const roomId = req.params.roomId
    const statusInfo = await this.checkDisconnect(roomId)
    // const disconnected = statusInfo.disconnected
    const currentDrawerId = statusInfo?.drawerId
    const nthDraw = statusInfo?.nthDraw
    const totalRounds = statusInfo?.totalRounds
    const remainingRounds = statusInfo?.remainingRounds
    const drawingStatus = statusInfo?.drawingStatus
    const guessWord = statusInfo?.guessWord
    const drawerIsDisconnect = statusInfo?.drawerIsDisconnect
    const endGame = statusInfo?.endGame
    if (endGame) {
      await this.gameService.endGame(roomId)
    }

    return res.json({ endGame: endGame, drawerId: currentDrawerId, drawerIsDisconnect:drawerIsDisconnect, nthDraw: nthDraw, totalRounds: totalRounds, remainingRounds: remainingRounds, drawingStatus: drawingStatus, guessWord: guessWord })
  }

  // triggered by start button
  startDrawing = async (req: Request, res: Response) => {
    const roomId = req.params.roomId
    await this.gameService.updateRoomStatus(roomId)
    await this.gameService.sendStartMsg(roomId)
    res.json({ 'start': 'start' }) // return guestItem word for drawer to draw
  }

  // fetched by client per second
  remainingTime = async (req: Request, res: Response) => {
    const roomId = req.params.roomId
    const remainingTimeInSec = await this.gameService.getRemainTime(roomId)
    let timeUp = false
    let correctWord = ''
    if (remainingTimeInSec <= 0) {
      timeUp = true
      correctWord = await this.gameService.getGuessingWord(roomId)
      await this.gameService.nextDrawingGame(roomId)
    }
    const timeInfo = { "timeUp": timeUp, "remainSec": remainingTimeInSec, "correctWord": correctWord }
    await this.gameService.sendRemainTimeInfoMsg(roomId, timeInfo)
    return res.json({ remainingSec: remainingTimeInSec });
  };

  guessChecking = async (req: Request, res: Response) => { // triggered by viewer enter guessing word
    const roomId = req.params.roomId
    // const userId = req.session["user"].id
    const userId = req.body.userId
    const userName = req.body.userName
    const guessedWord = req.body.word
    const guessingItem = await this.gameService.getGuessingWord(roomId)
    let guessingResult
    if (guessedWord.toLowerCase() === guessingItem.toLowerCase()) {
      guessingResult = { guess: 'true', guessUserId: userId, guessUserName: userName, itemAnswer: guessingItem }
      const remainSec = await this.gameService.getRemainTime(roomId)

      let data = await this.gameService.getRoomInfo(roomId)
      let participants = await this.gameService.getParticipants(roomId)
      let nthDraw = data[0]['nth_draw']
      let drawerNthPlayer = Math.floor(nthDraw % participants.length)
      let drawerId = participants[drawerNthPlayer].user_id
      await this.gameService.addScore(40, drawerId, remainSec, roomId, userId)
      await this.gameService.sendGuessCorrectMsg(roomId, guessingResult)
      await this.gameService.nextDrawingGame(roomId)
      return res.json(guessingResult)
    } else {
      return res.json({ guessUserId: userId, guess: 'false' })
    }
  };

  playerScores = async (req: Request, res: Response) => {
    const roomId = req.params.roomId
    const playerScores = (await this.gameService.getPlayerScores(roomId)).rows
    return res.json({ playerScores: playerScores })
  }

  // AIPrediction = async (req:Request, res: Response) => {
  //   const model = loadGuessModel()
  //   console.log("model loaded")
  //   return res.json({ message: model})
  // }

}