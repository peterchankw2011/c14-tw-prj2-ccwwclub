import { TimeInfo, GuessingResult} from './models';
import { Knex } from 'knex'
import { Server as SocketIO } from 'socket.io'


export class GameService {
    constructor(private knex: Knex, private io:SocketIO){}

    async getRandomDrawingItem(){
        const select = await this.knex.raw('SELECT english FROM words ORDER BY RANDOM() LIMIT 1');
        return select.rows[0].english
    }

    async getRemainTime(roomId: string){
        let select = await this.knex.raw(`SELECT extract(epoch from (SELECT (NOW() - started_at) AS duration FROM rooms WHERE id = ${roomId}))`);
        let durationInSec = Math.ceil(select['rows'][0]['date_part'])
        select = await this.knex.select('time_limit').from('rooms').where('id', roomId)
        let timeLimit = select[0].time_limit
        let remainingTimeInSec = timeLimit - durationInSec
        return remainingTimeInSec
    }

    async getGuessingWord(roomId:string){
        return (await this.knex.select('guess_item').from('rooms').where('id', roomId))[0].guess_item
    }

    async addScore(baseScore:number, drawerId:string, remainSec:number, roomId:string, userId:string){
        const addScore = baseScore + remainSec
        await this.knex.raw("UPDATE room_participants SET scores = scores + ? WHERE room_id = ? AND user_id = ?",[addScore, roomId, userId])
        await this.knex.raw("UPDATE users SET scores = scores + ? WHERE id = ?", [addScore, userId])
        await this.knex.raw("UPDATE room_participants SET scores = scores + ? WHERE room_id = ? AND user_id = ?",[baseScore, roomId, drawerId])
        return 'score added'
    }

    async nextDrawingGame(roomId:string){
        let guessWord = await this.getRandomDrawingItem()
        return await this.knex.raw("UPDATE rooms SET nth_draw = nth_draw + 1, status = 'waiting', guess_item = ? WHERE id = ?", [guessWord, roomId])
    }

    async getAllPlayers(roomId:string){
        const allPlayers = (await this.knex.raw(`SELECT users.id, users.nickname, users.pro_pic, room_participants.role, room_participants.scores
                                FROM users INNER JOIN room_participants
                                ON users.id = room_participants.user_id
                                WHERE room_participants.room_id = ?`, [roomId])).rows
        return allPlayers                        
    }

    async getRoomInfo(roomId:string){
        const roomInfo = await this.knex.select('nth_draw', 'round', 'status', 'guess_item').from('rooms').where('id', roomId)
        return roomInfo
    }

    async getParticipants(roomId:string){
        const participants = await this.knex.select('user_id','role').from('room_participants').where('room_id', roomId).orderBy('id')
        return participants
    }

    async updateRoomStatus(roomId:string){
        return await this.knex('rooms').where({ id: roomId }).update({ started_at: 'now()', status: 'drawing' })
    }

    async participantDisconnect(userId:string, roomId:string){
        await this.knex('room_participants').where({user_id: userId, room_id: roomId}).update({role: 'disconnected'})
        return 'Disconnect updated'
    }

    async participantReconnect(userId:string, roomId:string){
        await this.knex('room_participants').where({user_id: userId, room_id: roomId}).update({role: 'participant'})
        return 'Reconnect updated'
    }

    async getPlayerScores(roomId:string){
        // const playerScores = await this.knex.select('scores').from('room_participants').where('room_id',roomId).orderBy('scores')
        const playerScores = await this.knex.raw(`SELECT room_participants.scores, users.nickname FROM room_participants
                                                    INNER JOIN users ON room_participants.user_id = users.id
                                                    WHERE room_participants.room_id = ?
                                                    ORDER BY room_participants.scores DESC`,[roomId])
        return playerScores
    }

    async endGame(roomId:string){
        await this.knex('rooms').where({id: roomId}).update({status:'end'})
        return 'game ended'
    }

    async skipDisconnectedPlayer(roomId:string, nthDraw:string){
        await this.knex('rooms').where({id: roomId, nth_draw: nthDraw}).update({nth_draw: (parseInt(nthDraw)+1)})
        return 'skipped disconnected player'
    }

    async sendStartMsg(roomId:string){
        return this.io.to(`${roomId}`).emit("startDrawing", "startDrawing")
    }

    async sendRemainTimeInfoMsg(roomId:string,timeInfo: TimeInfo){
        // drawer: generate 'Next' Button pass to the next drawer, viewer: freeze & show answer
        return this.io.to(`${roomId}`).emit("remainTimeInfo", timeInfo) 
    }

    async sendGuessCorrectMsg (roomId: string, guessingResult: GuessingResult){
        // tell all roomates who guess correct, stop
        return this.io.to(`${roomId}`).emit("guessCorrect", guessingResult) 
    }

    async sendSkipDisconnectedPlayerMsg (roomId: string){
        return this.io.to(`${roomId}`).emit("skipDisconnect", 'player skipped')
    }
    


}