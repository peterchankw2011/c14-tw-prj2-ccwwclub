const roomId = document.location.href.split("?")[1].split("=")[1];
let colorKey = "1";
let base64_img = "";
let base64_gauGan = "";

let data;
let currentDrawerId = 0;
let guessWord = "";
let drawerId = 0;
let drawerName = "";
let isDrawer = false;
let timeInterval;
let streamInterval;
let playerInterval;
let nthDraw = 0;
let playerNumber = 0;
const userInfo = await loadUserInfo();
const userId = userInfo["id"];
const userName = userInfo["nickname"];
document.querySelector(".user-name").innerText = userName;

let playerList = {};



getUserInfo();

async function getUserInfo() {
  const res = await fetch(`/userInfo`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;

    const nickname = data.userInfo[0].nickname;
    const propic = data.userInfo[0].pro_pic;
    const user = document.querySelector(".user");
    user.innerHTML = `<img class="propic" src="../uploads/${propic}" />
    <div class="user-name">${nickname}</div>`;
  }
}

document.body.addEventListener("keydown", function (event) {
  if (event.keyCode == 189) {
    colorKey = "0";
  } else if (event.keyCode == 188) {
    colorKey = "1";
  } else if (event.keyCode == 190) {
    colorKey = "2";
  } else if (event.keyCode == 51) {
    colorKey = "3";
  }
});

document.querySelector("#lobby").addEventListener("click", () => {
  window.location = "/lobby";
});

window.onbeforeunload = async function () {
  console.log("leave");
  const res = await fetch(`/leaveRoom/${roomId}`);
  window.location = "/lobby"
  if (res.status === 200) {
    window.location = "/lobby";
  }
};

let playerStream;
const video = document.getElementById("monitor");
document.getElementById("monitor").hidden = true;

//Handles getUserMedia success
function gotMediaStream(mediaStream) {
  playerStream = mediaStream;
  video.srcObject = mediaStream;
  roomPageFirstLoad();
}

//error function for getUserMedia
function handleMediaStreamError(error) {
  console.log("navigator.getUserMedia error: ", error);
}

const mediaStreamConstraints = {
  video: { width: 360, height: 360 },
};

// Initializes media stream.
navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
  .then(gotMediaStream).catch(handleMediaStreamError);


/* //load player video stream
try {
  video.srcObject = await navigator.mediaDevices.getUserMedia({
    audio: false,
    video: { width: 360, height: 360 },
  });
  v
  await new Promise((resolve) => (video.onloadedmetadata = resolve)); // waiting for onloadmetadata as a promise parameter
  roomPageFirstLoad();
} catch (err) {
  console.log("cannot get camera");
}
 */

async function loadPlayerScores() {
  const res = await fetch(`/playerScores/${roomId}`)
  const playerScores = (await res.json()).playerScores
  let innerHTML = ''
  for (let scoreId in playerScores) {
    innerHTML += `<div class="rank">
                <div>${parseInt(scoreId) + 1}.</div>
                <div>${playerScores[scoreId].nickname}</div>
                <div>${Math.floor(playerScores[scoreId].scores)}</div>
              </div>
    `
  }
  document.querySelector('#ranks').innerHTML = innerHTML
}

loadPlayerScores()


async function loadPlayersInfo() {
  playerNumber = 0
  await loadPlayerScores()
  // load the page based on player's role
  const res = await fetch(`/roomPlayers/${roomId}`);
  const players = await res.json();

  let playersContainerHTML = "";
  let participantsHTML = "";
  let player = {};
  for (player of players) {
    // load players info
    playerList[player.id] = player.nickname;
    if (player.role == "creator") {
      playerNumber+=1
      // display creator first
      playersContainerHTML += addPlayerInfoBlock(
        false,
        false,
        player.id,
        player.pro_pic,
        player.nickname,
        player.scores
      ); // add room creator's info first
    } else {
      if (player.role == "disconnected") {
        participantsHTML += addPlayerInfoBlock(
          false,
          true,
          player.id,
          player.pro_pic,
          player.nickname,
          player.scores
        );
      } else {
        playerNumber+=1
        participantsHTML += addPlayerInfoBlock(
          false,
          false,
          player.id,
          player.pro_pic,
          player.nickname,
          player.scores
        );
      }
    }
  }
  playersContainerHTML += participantsHTML;
  document.getElementById("playersContainer").innerHTML = playersContainerHTML;

  clearInterval(playerInterval);
  socket.on("playerCamera", (cameraInfo) => {
    drawPlayerCamera(cameraInfo.userId, cameraInfo.playerCameraURL);
  })
  playerInterval = setInterval(()=>{sendPlayerCamera()}, 200);
}

async function loadDrawingStatusInfo() {
  console.log("this is a test");
  const res = await fetch(`/currentDrawingStatus/${roomId}`);
  const drawingStatusInfo = await res.json();
  console.log(drawingStatusInfo);
  drawerId = drawingStatusInfo.drawerId;
  isDrawer = drawerId === userId;
  nthDraw = drawingStatusInfo.nthDraw;
  if (drawingStatusInfo.drawerIsDisconnect){
    const res = await fetch(`/skipDisconnectedPlayer/${roomId}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        nthDraw: nthDraw
      }),
    });
    console.log(await res.json())
  }
  const totalRounds = drawingStatusInfo.totalRounds;
  const remainingRounds = drawingStatusInfo.remainingRounds;
  const currentRound = totalRounds - remainingRounds;
  const drawingStatus = drawingStatusInfo.drawingStatus;
  const endGame = drawingStatusInfo.endGame;

  if (endGame) {
    console.log("gameEnd");
    document.querySelector('#rules').innerText = 'Game Ended'
    document.querySelector('#rules').hidden = false
  } else {
    document.querySelector(
      "#round"
    ).innerHTML = `Round:<br>${currentRound}/${totalRounds}`;
    document.querySelector(
      "#countDown"
    ).innerHTML = `Next:<br>${playerList[drawerId]}`;

    if (drawingStatus === "pending" || drawingStatus === "waiting") {
      // load start or wait button
      let guessContainer = document.getElementById("guessContainer");
      guessContainer.innerHTML = ''
      if (isDrawer) {
        if(playerNumber>1){
          guessContainer.innerHTML = `<div id="startDrawing">Draw</div>`;
        }
        startButtonEventListener();
      } else {
        guessContainer.innerHTML = `<div id="waitingGame">Waiting</div>`;
      }
    } else if (drawingStatus === "drawing") {
      // load streaming
      let guessContainer = document.querySelector("#guessContainer");
      const guessWord = drawingStatusInfo.guessWord;
      if (isDrawer) {
        guessContainer.innerHTML = `<div id="guessItem">${guessWord}</div>`;
      } else {
        let hint = ''
        for (let i = 0; i < guessWord.length; i++) {
          if (i == 0) {
            hint += guessWord[i] + ' '
          } else if (guessWord[i] == ' ') {
            hint += '   '
          } else {
            hint += '_ '
          }
        }
        console.log('hint' + hint)
        guessContainer.innerHTML = `<form id="guess-form">
                                      <input type='text' placeholder="${hint}" name="guessWord">
                                      <input type="submit" value="Guess" class="button">
                                    </form>`;
        submitGuessListener();
      }
      streamDrawUpdating(true, isDrawer);
    }
    if (document.querySelector(".drawer")) {
      document.querySelector(".drawer").classList.remove("drawer");
    }
    console.log(drawerId + " is drawer")
    document.querySelector(`[playerid="${drawerId}"]`).classList.add("drawer");
  }
}

async function roomPageFirstLoad() {
  await loadPlayersInfo();
  await loadDrawingStatusInfo();
}

function addPlayerInfoBlock(drawer, disconnected, id, proPic, nickname, scores) {
  return `<div class="player ${drawer ? 'drawer' : ''} ${disconnected ? 'disconnected' : ''}" playerid=${id}>
            <canvas class="smallStream" width="150" height="150" ></canvas>
            <div class="playerInfo">
              <img class="propic" src="../uploads/${proPic}" />
              <div class="playerName">${nickname}</div>
              <div class="playerScore">${disconnected ? "Disconnected" : `Scores: ${Math.floor(scores)}`
    }</div>
            </div>
          </div>`;
}

function startButtonEventListener() {
  if (document.getElementById("startDrawing")){
    document
      .getElementById("startDrawing")
      .addEventListener("click", async function (event) {
        socket.emit("createSubmit", "activated refresh lobby");
        console.log("pressed start game");
        const res = await fetch(`/startDrawing/${roomId}`);
        if (res.status == 200) {
          console.log("game started");
        }
      });
  }
}

// drawer stream his/her drawing
async function streamDrawing() {
  try {
    // video.srcObject = await navigator.mediaDevices.getUserMedia({
    //   video: { width: 720, height: 720 },
    // });
    // await new Promise((resolve) => (video.onloadedmetadata = resolve)); // waiting for onloadmetadata as a promise parameter
    const canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    document.getElementById("splash").hidden = true;
    document.getElementById("app").hidden = false;

    canvas.getContext("2d").drawImage(video, 0, 0);
    const dataURL = canvas.toDataURL();
    const res = await fetch("http://localhost:8000/paint", {
    // const res = await fetch("https://python.ccww.club/paint", {
    // const res = await fetch("https://tecky.hikefriends.com/paint", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        img: dataURL,
        drawerId: currentDrawerId,
        roomId: roomId,
        nthDraw: nthDraw,
        colorKey: colorKey,
        timeLimit: 30,
        gauGan: true,
      }),
    });
    if (res.status === 200) {
      const response = await res.json();

      base64_img = "data:image/png;base64," + response["img_base64"][0];
      document.getElementById("drawnImage").src = base64_img;

      base64_ccww = "data:image/png;base64," + response["img_base64"][1];
      let imageCCWW = new Image();
      canvasCCWW.getContext('2d').drawImage(imageCCWW, 0, 0, imageCCWW.width, imageCCWW.height, 0, 0, canvasCCWW.width, canvasCCWW.height)
      imageCCWW.src = base64_ccww;

      document.getElementById("ccwwlogo").src = base64_ccww;

    }
  } catch (err) {
    console.error(err);
  }
}

// viewer view drawer's streaming his/her draw
async function viewDrawing() {
  try {
    const res = await fetch("http://localhost:8000/view", {
    // const res = await fetch("https://python.ccww.club/view", {
    // const res = await fetch("https://tecky.hikefriends.com/view", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        drawerId: currentDrawerId,
        roomId: roomId,
        nthDraw: nthDraw,
      }),
    });

    if (res.status === 200) {
      const response = await res.json();
      base64_img = "data:image/png;base64," + response["img_base64"];
      document.getElementById("drawnImage").src = base64_img;
    }
  } catch (err) {
    console.error(err);
  }
}

function timeUpEvent(answer) {
  streamDrawUpdating(false, isDrawer);
  if (isDrawer) {
    getRemainSec(false);
  } else {
    document.querySelector(
      "#guessContainer"
    ).innerHTML = `<div id="guessItem">Answer: ${answer}</div>`;
  }
  loadNextDrawingCountDown(3);
}

function loadNextDrawingCountDown(seconds) {
  let countdown = seconds;
  let nextDrawerCountdown = setInterval(() => {
    document.querySelector(
      "#countDown"
    ).innerHTML = `Next:<br>in ${countdown}s`;
    countdown -= 1;
  }, 1000);
  setTimeout(async () => {
    clearInterval(nextDrawerCountdown);
    await loadPlayersInfo()
    await loadDrawingStatusInfo();
  }, 3000);
}

function submitGuessListener() {
  document
    .querySelector("#guess-form")
    .addEventListener("submit", async function (event) {
      console.log("guess");
      event.preventDefault();
      const form = event.target;
      const word = form.guessWord.value;
      const res = await fetch(`/guessChecking/${roomId}`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          userId: userId,
          userName: userName,
          word: word,
        }),
      });
      //emit message to server
      socket.emit("guessMessage", {
        guesserId: userId,
        guesserName: userName,
        word: word,
      });

      //clear input
      event.target.guessWord.value = "";
      event.target.guessWord.focus();

      const guessResult = (await res.json()).guess;
      if (guessResult == "true") {
        console.log("your guess correct");
      }
    });
}

// triggered after socket getting news of some guess correctly or time out
function guessCorrectEvent(answer, guesserId, guesserName) {
  loadPlayerScores()
  streamDrawUpdating(false, isDrawer);
  if (true) {
    if (userId === guesserId) {
      document.querySelector(
        "#guessContainer"
      ).innerHTML = `<div id="guessItem">${answer} is correct</div>`;
    } else {
      document.querySelector(
        "#guessContainer"
      ).innerHTML = `<div id="guessItem">${guesserName} correct<br>Answer: ${answer}</div>`;
    }
  }
  loadNextDrawingCountDown(3);
}

// socket events
const socket = io();
socket.emit("joinRoom", roomId);

// socket.on("roomReconnect", (data) => {
//   console.log(data.connectMsg);
//   loadPlayersInfo();
// });

// socket.on("roomDisconnect", (data) => {
//   console.log(data.disconnectMsg);
//   loadPlayersInfo();
// });

socket.on("remainTimeInfo", (timeInfo) => {
  let timeUp = timeInfo.timeUp;
  let answer = timeInfo.correctWord;
  if (timeUp) {
    timeUpEvent(answer);
  } else {
    let remainSec = timeInfo.remainSec;
    let minute = Math.floor(remainSec / 60);
    let sec = remainSec % 60;
    document.querySelector(
      "#countDown"
    ).innerHTML = `Time:<br>${minute}:${sec}`;
  }
});

socket.on("startDrawing", async (data) => {
  await loadPlayersInfo();
  await loadDrawingStatusInfo();
  document.getElementById("rules").hidden = true;
});

socket.on("guessCorrect", (correctGuess) => {
  const correctUserId = correctGuess.guessUserId;
  const userName = correctGuess.guessUserName;
  const correctAnswer = correctGuess.itemAnswer;
  guessCorrectEvent(correctAnswer, correctUserId, userName);
  getRemainSec(false);
});


function drawPlayerCamera(userId, camUrl) {
  const canvasPlayer = document.querySelector(
    `[playerid="${userId}"] > .smallStream`
  );
  const image = new Image();
  const ctx = canvasPlayer.getContext("2d");
  image.src = camUrl;
  image.addEventListener("load", () => {
    ctx.drawImage(image, 0, 0, 150, 150);
  });
}

socket.on("joinRoomPlayer", async (userId) => {
  console.log(userId + " join");
  await loadPlayersInfo();
  await loadDrawingStatusInfo()
});

socket.on("leaveRoomPlayer", async (userId) => {
  console.log(userId + " left");
  await loadPlayersInfo();
  await loadDrawingStatusInfo()
});

socket.on("forceAllLeave", () => {
  window.location = "/lobby";
  socket.emit("createSubmit", "activated refresh lobby");
});

socket.on("skipDisconnect", async (data) => {
  console.log('skipped disconnected player'+data)
  await loadPlayersInfo();
  await loadDrawingStatusInfo();
  document.getElementById("rules").hidden = true;
})

function getRemainSec(start) {
  clearInterval(timeInterval);
  if (start) {
    timeInterval = setInterval(async () => {
      await fetch(`/remainingTime/${roomId}`); // activate server socket to broadcast time
    }, 1000);
  }
}

// getRemainSec()

function sendPlayerCamera() {
  const canvasPlayer = document.createElement("canvas");
  canvasPlayer.width = 150;
  canvasPlayer.height = 150;
  canvasPlayer.getContext("2d").drawImage(video, 0, 0, 150, 150);
  const dataURL = canvasPlayer.toDataURL();
  //console.log(dataURL)
  socket.emit("playerCamera", { userId: userId, playerCameraURL: dataURL });
}

function streamDrawUpdating(start, isDrawer) {
  clearInterval(streamInterval);
  if (start) {
    if (isDrawer) {
      getRemainSec(true);
    }
    if (isDrawer) {
      streamInterval = setInterval(async () => {
        streamDrawing();
      }, 200);
    } else {
      streamInterval = setInterval(() => {
        viewDrawing();
      }, 200);
    }
  }
}

async function loadUserInfo() {
  const nameContainer = document.querySelector("#userName");
  const res = await fetch("/userInfo");
  const result = await res.json();
  if (res.status === 200) {
    console.log(result);
    // data = result;
    // console.log(data.id + '!')
    // console.log(data.nickname)
    // nameContainer.innerText = `${data.nickname}`

    return result["userInfo"][0];
  } else {
    console.log("failed");
    console.log("no info");
  }
}

////PETER WORKING!!!!!!!!!
//receiving message from server
socket.on("message", (message) => {
  console.log(message);
  if (message.guesserId === userId) {
    outputMessage(message);
  } else {
    outputMessageFromCounterpart(message);
  }

  //scroll down auto
  messageSpace.scrollTop = messageSpace.scrollHeight;
});

const messageSpace = document.querySelector(".message-space");

function outputMessage(message) {
  const div = document.createElement("div");
  div.classList.add("user");
  div.innerHTML = `
  <div class="user-msg" >
    ${message.word}
  </div>`;
  messageSpace.appendChild(div);
}

function outputMessageFromCounterpart(message) {
  const div = document.createElement("div");
  div.classList.add("counterpart");
  div.innerHTML = ` 
    <div class="counterpart-msg">
    ${message.guesserName}: <br />
      ${message.word}
    </div>
  `;
  messageSpace.appendChild(div);
}

async function sendingPictureToAI(){
  const res = await fetch('/AIPrediction');
  const result = await res.json(); 
  if (res.status === 200) {
    console.log(result.message)
    return data
  }
}

sendingPictureToAI();