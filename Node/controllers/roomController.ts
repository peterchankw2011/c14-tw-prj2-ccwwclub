import { Request, Response } from "express";
import { RoomService } from "../services/roomService";

export class RoomController {
  constructor(private roomService: RoomService) {}

  allRooms = async (req: Request, res: Response) => {
    const room = await this.roomService.getAllRooms();
    res.json(room);
  };

  createRoom = async (req: Request, res: Response) => {
    const userFound = req.session["user"];
    //console.log(userFound.id);
    const room = req.body;
    //console.log(room);
    await this.roomService.postRoom(room, userFound.id);
    res.json({ success: true });
  };

  checkPW = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);
    const clientPw = req.body;
    const pwFromDB = await this.roomService.getRoomPW(id);
    console.log(pwFromDB.rows[0]);
    if (clientPw.password === pwFromDB.rows[0].password) {
      res.json({ success: true });
    } else {
      res.json({ success: false });
    }
  };

  userJoinRoom = async (req: Request, res: Response) => {
    const userFound = req.session["user"];
    const id = parseInt(req.params.id);
    await this.roomService.getUserJoinRoom(id, userFound.id);
    res.json({ success: true });
  };

  joinRoomAfterCreate = async (req: Request, res: Response) => {
    const userFound = req.session["user"];
    const roomId = await this.roomService.getRoomIdAfterCreate(userFound.id);
    res.json(roomId);
  };
  
  leaveRoom = async (req: Request, res: Response) => {
    const userId = req.session["user"].id
    const roomId = req.params.roomId
    await this.roomService.disconnectedParticipant(roomId, userId)
  }

  getPartiInRoom = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);
    const partiNumber = await this.roomService.getPartiNumber(id);
    res.json(partiNumber);
  };

  getAccommodation = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);
    const accommodation = await this.roomService.getAccommodation(id);
    res.json(accommodation);
  };
}
