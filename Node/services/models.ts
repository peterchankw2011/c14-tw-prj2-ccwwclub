export interface UserLogin {
    email: string,
    password: string
}


export interface User {
    email: string,
    password: string,
    nickname: string,
    pro_pic?: string,
    scores?: number,
    timestamp?: boolean
}

export interface TimeInfo {
    timeUp: boolean,
    remainSec: number,
    correctWord: string
}

export interface GuessingResult {
    guess: string,
    guessUserId: string,
    guessUserName: string,
    itemAnswer: string
}

export interface RoomInfo {
    id: string,
    roomName: string,
    enableAI: string,
    numberOfPeople: string,
    roundsPerPerson: number,
    password: string,
}

export interface GuessMessage {
    guesserId: string,
    guesserName: string,
    word:string
}