import { RoomInfo } from "./models";
import { Knex } from "knex";
import { Server as SocketIO } from "socket.io";


export class RoomService {
  constructor(private knex: Knex, private io: SocketIO) {}

  async getAllRooms() {
    const room = await this.knex.raw(
      `select rooms.id as roomid, rooms.name as name, users.nickname as nickname, users.pro_pic as propic, rooms.mode as mode, rooms.accommodation as accommodation, rooms.password as password, rooms.round as round from rooms left join users on rooms.creator_id = users.id where rooms.status = 'pending' order by rooms.created_at desc`
    );
    return room;
  }

  async postRoom(room: RoomInfo, userId: number) {
    const guessWord = (await this.knex.raw('SELECT english FROM words ORDER BY RANDOM() LIMIT 1')).rows[0].english
    return await this.knex.raw(
      `INSERT INTO rooms (nth_draw,time_limit,creator_id,name,mode,accommodation,round,password,created_at,status,guess_item) VALUES (?,?,?,?,?,?,?,?,NOW(),?,?) `,
      [
        0,
        60,
        userId,
        room.roomName,
        room.enableAI,
        room.numberOfPeople,
        room.roundsPerPerson,
        room.password,
        "pending",
        guessWord
      ]
    );
  }

  async getRoomPW(id: number) {
    const pwFromDB = await this.knex.raw(
      `select password from rooms where id = ${id}`
    );
    return pwFromDB;
  }

  async getUserJoinRoom(id: number, userId: number) {
    await this.knex.raw(
      `insert into room_participants (room_id,user_id,role,scores) values (?,?,?,?)`,
      [id, userId, "participant",0]
    );
    this.sendJoinRoomMsg(userId, id);
  }

  async getRoomIdAfterCreate(userId: number) {
    const roomId = await this.knex.raw(
      `select id from rooms where creator_id = ${userId} order by created_at DESC limit 1`
    );
    await this.knex.raw(
      `insert into room_participants (room_id,user_id,role,scores) values (?,?,?,?)`,
      [roomId.rows[0].id, userId, "creator",0]
    );
    return roomId;
  }

  async disconnectedParticipant(roomId: string, userId: string) {
    // await this.knex('room_participants').where({'id': roomId, 'user_id': userId}).del()
    const isCreatorOrNot = await this.knex.raw(
      `select role from room_participants  WHERE room_id = ? AND user_id = ?`,
      [roomId, userId]
    );
    const roomStatus = (
      await this.knex.raw(`SELECT status FROM rooms WHERE id = ?`, [roomId])
    ).rows[0].status;
    if (isCreatorOrNot.rows[0].role === "creator") {
      await this.knex.raw("DELETE FROM room_participants WHERE room_id = ?", [
        roomId,
      ]);
      await this.knex.raw("DELETE FROM rooms where id = ?", [roomId]);
      await this.allLeaveRoom(userId, roomId);
    }
    if (isCreatorOrNot.rows[0].role === "participant") {
      if (roomStatus === "pending") {
        await this.knex.raw(
          "DELETE FROM room_participants WHERE room_id = ? AND user_id = ?",
          [roomId, userId]
        );
      } else {
        await this.knex("room_participants")
          .where({ room_id: roomId, user_id: userId })
          .update({ role: "disconnected" });
      }
    }
    this.sendLeaveRoomMsg(userId, roomId);
  }

  async sendJoinRoomMsg(userId: number, roomId: number) {
    // drawer: generate 'Next' Button pass to the next drawer, viewer: freeze & show answer
    return this.io.to(`${roomId}`).emit("joinRoomPlayer", userId);
  }

  async allLeaveRoom(userId: string, roomId: string) {
    // drawer: generate 'Next' Button pass to the next drawer, viewer: freeze & show answer
    return this.io.to(`${roomId}`).emit("forceAllLeave", userId);
  }

  async sendLeaveRoomMsg(userId: string, roomId: string) {
    // drawer: generate 'Next' Button pass to the next drawer, viewer: freeze & show answer
    return this.io.to(`${roomId}`).emit("leaveRoomPlayer", userId);
  }

  async getPartiNumber(roomId: number) {
    const partiNumber = await this.knex.raw(
      `select COUNT(room_id) from room_participants where room_id = ${roomId}`
    );
    return partiNumber;
  }
  async getAccommodation(roomId: number) {
    const accommodation = await this.knex.raw(
      `select accommodation from rooms where id = ${roomId}`
    );
    return accommodation;
  }
}
