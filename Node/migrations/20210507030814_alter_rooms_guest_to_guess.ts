import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('rooms',(table)=>{
        table.renameColumn('guest_item','guess_item');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('rooms',(table)=>{
        table.renameColumn('guess_item','guest_item');
    })
}

