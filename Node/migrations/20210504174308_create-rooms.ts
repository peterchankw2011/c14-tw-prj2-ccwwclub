import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('rooms', table=>{
        table.increments();
        table.integer('creatorId').references('id').inTable('users')
        table.string('name',255);
        table.string('password',255);
        table.string('description',255);
        table.integer('accommodation');
        table.string('status',255)
        table.string('mode',255)
        table.timestamps(false,true);
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('rooms');
}

