import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('users', table=>{
        table.increments();
        table.string('email',255);
        table.string('password',255);
        table.string('nickname',255);
        table.string('pro_pic',255);
        table.decimal('scores',5,2);
        table.timestamps(false,true);
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('users');
}

